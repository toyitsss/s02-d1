------ Git Repository -------

Git  - is a version control system that manages the revisions/changes/updates of our folder/respository


To create a git repository for our projects folders:

1. Open your terminal/git bash on your project, then run command:

$ git init

-git init is short for git initialized. This will create a git repository that will handle the revisions and histories of your project.

2. On your terminal, run command:

$ git status

-git status will return a message of all the untracked files (changes/revisions) made on our project

3. Then run:

$ git add .

-git add . - adds all the untracked files on the staging area before saving it on our git repository (waiting area/polishing area/finalized files)

$ git commit -m ""

-git commit -m is a way of saving changes/revisions we have nade on our project
- -m (flag) shot for message